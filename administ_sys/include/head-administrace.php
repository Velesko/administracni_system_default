    <title>Web Of Something</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="../Style/default.css" rel="stylesheet" type="text/css"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="../js/Google-maps.js"></script>
    <script src="../js/gallery.js"></script>
    <script src="../js/webgallery.js"></script>
    <script src="../js/error.js"></script>
    <script src="../js/ckeditor/ckeditor.js"></script>
    <script src="../js/ckeditor/adapters/jquery.js"></script>
    <script src="../js/login.js"></script>
    <script src="../js/opacity.js"></script>
    <script type="text/javascript">
        $(function() {
            $('#otevri').hover(function() {
                $('#advanced').stop().slideDown(300);
            });
        });

        $(function() {
            $('#otevri').mouseleave(function() {
                $('#advanced').stop().slideUp(300);
            });
        });
    </script>