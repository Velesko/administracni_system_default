<?php

if ($_GET['chyba'] == 1) {
    echo '
        <div class="error"><p>Nepodařilo se odeslat požadavek, pracujeme na tom.</p></div>
';
}

if ($_GET['chyba'] == 2) {
    echo '
        <div class="success"><p>Odeslání proběhlo úspěšně</p></div>
';
}

if ($_GET['chyba'] == 3) {
    echo '
        <div class="error"><p>Critical ERROR: Nepodařilo se připojit k databázi</p></div>
';
}

if ($_GET['chyba'] == 4) {
    echo '
        <div class="success"><p>Jste úspěšně zaregistrováni</p></div>
';
}

if ($_GET['chyba'] == 5) {
    echo '
        <div class="error"><p>Registrace se nezdařila</p></div>
';
}

if ($_GET['chyba'] == 6) {
    echo '
        <div class="error"><p>Nebyli vyplněny všechny povinné údaje</p></div>
';
}

if ($_GET['chyba'] == 7) {
    echo '
        <div class="error"><p>Hesla se neshodují</p></div>
';
}

if ($_GET['chyba'] == 8) {
    echo '
        <div class="error"><p>Zadané jméno nebo email je již používán někým jiným!</p></div>
';
}

if ($_GET['chyba'] == 9) {
    echo '
        <div class="error"><p>Špatně zadané jméno nebo heslo</p></div>
';
}

if ($_GET['chyba'] == 10) {
    echo '
        <div class="success"><p>Jste úspěšně přihlášen</p></div>
';
}

if ($_GET['chyba'] == 11) {
    echo '
        <div class="success"><p>Jste úspěšně odhlášen</p></div>
';
}

if ($_GET['chyba'] == 12) {
    echo '
        <div class="error"><p>Nejste přihlášen</p></div>
';
}

if ($_GET['chyba'] == 13) {
    echo '
        <div class="error"><p>Na toto nemáte oprávnění!</p></div>
';
}

if ($_GET['chyba'] == 14) {
    echo '
        <div class="success"><p>Úspěšně odesláno do databáze</p></div>
';
}

if ($_GET['chyba'] == 15) {
    echo '
        <div class="error"><p>Někde se stala chybka</p></div>
';
}

if ($_GET['chyba'] == 16) {
    echo '
        <div class="success"><p>Záznam byl úspěšně odstraněn</p></div>
';
}

if ($_GET['chyba'] == 17) {
    echo '
        <div class="success"><p>Vítejte zpět!</p></div>
';
}

if ($_GET['chyba'] == 18) {
    echo '
        <div class="success"><p>E-mail byl odeslán</p></div>
';
}

if ($_GET['chyba'] == 19) {
    echo '
        <div class="error"><p>E-mail nebyl odeslán</p></div>
';
}

if ($_GET['chyba'] == 20) {
    echo '
        <div class="error"><p>Nebyla vyplněna všechna pole</p></div>
';
}
?>