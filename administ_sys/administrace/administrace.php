
<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php
        include '../include/head-administrace.php';
        echo '<link rel="icon" href="../pics/administrace.png" type="image/x-icon" />'
        ?>
        <link href="../Style/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="../Style/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    </head>
    <?php
    include '../include/error.php'
    ?>
    <body>

        <div class="body" style="margin-top: 5px;">
            <div class="menu_admin">
                <ul>
                    <?php
                    if (($_SESSION['Userprav'] == 3) or ($_SESSION['Userprav'] == 2)) {
                    echo '
                    <a href="administrace.php"><li>Domů</li></a>
                    <a href="?strana=11"><li>Články</li></a>
                    <a href="?strana=9"><li>Registrace uživatelů</li></a>
                    <a href="?strana=10"><li>Seznam uživatelů</li></a>
                    <a href="?logout=yes"><li>Odhlásit</li></a>
                    <a href="../index.php"><li>Odejít</li></a>';
                    } 
                    else {
                    echo '
                    <a href="administrace.php"><li>Domů</li></a>
                    <a href="?strana=7"><li>Články</li></a>
                    <a href="?logout=yes"><li>Odhlásit</li></a>
                    <a href="../index.php"><li>Odejít</li></a>';
                    }
                    ?>
                </ul>
            </div>
            <div class="hltext_admin">
                <?php
                if ($_GET['strana'] == 7) {
                include 'clanky.php';
                } else if ($_GET['strana'] == 9) {
                include 'registrace.php';
                } else if ($_GET['strana'] == 10) {
                include 'seznamuz.php';
                } else if ($_GET['strana'] == 11) {
                include 'clanky_info.php';
                } else if ($_GET['strana'] == 12) {
                include 'edit_clanky.php';
                } else if ($_GET['strana'] == 13) {
                include 'edit_uziv_super.php';
                } else if ($_GET['logout'] == "yes") {
                Session_Start();      // Pokud chceme pracovat se session, musíme je "nastartovat". I pokud je chceme zničit.
                Session_Destroy();
                header("location: ../index.php?chyba=11");
                } else {
                include 'info.php';
                }
                ?>
            </div>
        </div>
    </body>
</html>
