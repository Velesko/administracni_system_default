$(document).ready(function() {
    
    $("#zobrazit").click(function() {
        var winheight = $("body").scrollTop();
        var scrollwinheight = winheight + 100;
        var height = $('html').height();
        var width = $('html').width();
        var scrolexit = winheight + 267;
        var sirkaOkna = $('html').width();
        var delkaTextu = $("#textbox-panel").width();
        $("#textbox").css({
            "background-color": "black",
            "width": width + "px",
            "height": height + "px"
        });
        $("#textbox-panel").css({
            "top": scrollwinheight + "px",
            "position": "absolute",
            "left": (sirkaOkna / 2 - delkaTextu / 2) + "px"
        });
        $("#textbox-panel").css("top", scrollwinheight + "px");
        $("#textbox-panel").css("left", (sirkaOkna / 2) - (delkaTextu / 2));
    });
    $("#zobrazit").click(function() {
        $("#textbox").slideDown(300);
        $("#textbox-panel").fadeIn(300);
    });
    $("a#close-panel").click(function() {
        $("#textbox, #textbox-panel").fadeOut(300);
    });
    $("#textbox").click(function() {
        $("#textbox, #textbox-panel").fadeOut(300);
    });
});