<?php


/** ---------- nastaveni ---------- */

$options = array(
                    'rows_total' => 100,  // celkovy pocet zaznamu 
                    'offset'     => 1, // po kolika budeme strankovat
                    'interval'   => 4, // interval cisel nalevo a napravo od aktivniho = vybraneho cisla
                    'url'        => '?' . $cislo . '&url=1&', // adresa ke ktere se pripoji strankovani parametr
                );
echo pagingBlock($options);


/**
 * Strankovani s promenlivym stredem 
 * 
 * @param array $options  pole slouzici k nastaveni hodnot ktere funkce vyzaduje viz priklad  
 * @date 28-12-2009  
 * @author Roman Janko <admin@rjwebdesign.net>  
 *   
 **/  
function pagingBlock($options = array())
{
    $html = '';
    
    $pages_total = ceil($options['rows_total'] / $options['offset']);
    $interval    = $options['interval'];
    $actual_page = empty($_GET["s"]) ? 1 : $_GET["s"];
    $url         = $options['url']; //"?url=1&";
    
    
    /** ---------- fix proti URL hacku ---------- */
    if ($actual_page < 0)     
        $actual_page = 1;
    
    if ($actual_page > $pages_total) 
        $actual_page = $pages_total;


    /** ---------- co budeme ukazovat, interval ---------- */
    $show = array();
    
    for ($i = $actual_page - $interval; $i <= $actual_page + $interval; $i++)
    {
        if ($i > 0 && $i <= $pages_total)
            $show[] = $i;
    }
    
    
    /** ---------- zacatek rady ---------- */
    
    if ($actual_page - $interval == 2)
    {
        $html .= "<a href='".$url."s=1'>1</a> ";
    }
    
    if ($actual_page - $interval > 2)
    {
        $html .= "<a href='".$url."s=1'>1</a> ... ";
    }
    
    
    /** ---------- dynamicky stred ---------- */
    
    for ($i = 1; $i <= $pages_total; $i++)
    {
        if (in_array($i, $show))
        {
            if ($i == $actual_page)
            {
                $html .= " <strong>$i</strong> ";
            }
            else
            {
                $html .= "<a href='".$url."s=$i'>$i</a> ";
            }
        }
    }
    
    /** ---------- konec rady ---------- */
    
    if ($actual_page + $interval + 1 == $pages_total)
    {
        $html .= "  <a href='".$url."s=".$pages_total."'>".$pages_total."</a>";
    }
    
    if ($actual_page + $interval + 1 < $pages_total)
    {
        $html .= " ... <a href='".$url."s=".$pages_total."'>".$pages_total."</a>";
    }
    
    
    /** ---------- vystup  ---------- */
    return $html;
}

?>