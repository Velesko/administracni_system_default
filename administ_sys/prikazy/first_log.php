<?php

ob_start();               // cachujeme vystup
if (isset($_POST['sent'])) {      // pokud byl odeslan formular pokracuj timto
    $jmeno = $_POST['jmeno'];
    $heslo = $_POST['heslo'];
    $database = $_POST['database'];

    if ($jmeno == "" or $database == "") { // pokud nebylo vyplněno něco z toho, co je povinné, dáme vědět a skript ukončíme
        header("location: ../administrace/index.php?chyba=6");
    }

    $obsah = "$jmeno//$heslo//$database";
    $soubor = fopen("../prikazy/conf/log", "a+");
    fwrite($soubor, $obsah);
    fclose($soubor);
    header("location: ../index.php");
} else {
    header("location: ../administrace/index.php?chyba=6");
}